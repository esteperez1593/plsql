--------------------------------------------------------
-- Archivo creado  - mi�rcoles-octubre-30-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package PACKAGE1
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "MPEREZFI"."PACKAGE1" AS 

TYPE pzcrcod_data IS RECORD (
    PZCRCOD_CODE	VARCHAR2(6 BYTE),
    PZCRCOD_NAME	VARCHAR2(144 BYTE),
    PZCRCOD_DESCRIPTION	VARCHAR2(144 BYTE),
    PZCRCOD_CODE_SUP	VARCHAR2(6 BYTE),
    PZCRCOD_USER	VARCHAR2(144 BYTE),
    PZCRCOD_ACTIVITY_DATE	DATE,
    PZCRCOD_DATA_ORIGIN	VARCHAR2(144 BYTE)
  );

 function get_pzcrcod_name
(
    empname VARCHAR2
)
return varchar2;

function get_pzcrcod_data_row
return sys_refcursor;

--Suma de porcentajes asociados a un perfil
function get_pzvpdef_sum
(
 profile_name VARCHAR2
)
return VARCHAR2;

function get_pzvpdef_count
(
 profile_name VARCHAR2
)
return numeric;

--Suma el porcentaje de objetivos registrados
function get_pzvadef_sum
(
 name_inst VARCHAR2, pidm NUMERIC
)
return numeric;

--Cuenta los objetivos Registrados
function get_pzvadef_count
(
 name_inst VARCHAR2, pidm NUMERIC
)
return numeric;

function get_percent_pzvadef_obj_reg(name_inst VARCHAR2,
                                     pidm NUMERIC)
return VARCHAR2;


PROCEDURE calc_sum_media(pidm numeric,
                                 instr_code VARCHAR2,
                                 calendar VARCHAR2);
                                 
PROCEDURE calc_percent_data_obj(pidm numeric,
                                 instr_code VARCHAR2,
                                 resp_id numeric );

function get_pzvpdef_data
(
 name_inst VARCHAR2, pidm NUMERIC,profile_us VARCHAR2
)
return NUMERIC;
END PACKAGE1;

/
