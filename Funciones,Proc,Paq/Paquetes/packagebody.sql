--------------------------------------------------------
-- Archivo creado  - mi�rcoles-octubre-30-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body PACKAGE1
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "MPEREZFI"."PACKAGE1" IS

 function get_pzcrcod_name
(
    empname VARCHAR2
    
)
return VARCHAR2
is
ret varchar2(50);

begin

select pzcrcod_name into ret 
from pzcrcod
where pzcrcod_CODE=empname;
return ret;
end;

FUNCTION get_pzcrcod_data_row return    sys_refcursor  
 
 IS
 
 TYPE ref_cursor_cargos IS REF CURSOR;
 
 v_ref_car ref_cursor_cargos;
 
 
function get_pzvpdef_sum
(
 profile_name VARCHAR2
)
return VARCHAR2
is
ret varchar2(200);
code_pzrpdef VARCHAR2(6);
total numeric;

begin

    select  pzrpdef.pzrpdef_code into code_pzrpdef
    from pzrpdef
    where pzrpdef_name = profile_name;
    
    select SUM(pzvpdef_value) into total 
    from pzvpdef,pzrpdef
    where pzvpdef.pzrpdef_code=code_pzrpdef
    and pzrpdef.pzrpdef_code = pzvpdef.pzrpdef_code;

 ret := 'Para el perfil: '||profile_name||' Se registro el: '||TO_CHAR(total, '9999.99')||'%';

return ret;

end get_pzvpdef_sum;
 
  
 BEGIN
   
     OPEN v_ref_car for SELECT * from pzcrcod;
     RETURN v_ref_car;
 
 END get_pzcrcod_data_row;

 function get_pzvpdef_sum
(
 profile_name VARCHAR2
)
return VARCHAR2
is
ret varchar2(200);
code_pzrpdef VARCHAR2(6);
total numeric;

begin

select  pzrpdef.pzrpdef_code into code_pzrpdef
from pzrpdef
where pzrpdef_name = profile_name;

select SUM(pzvpdef_value) into total 
from pzvpdef,pzrpdef
where pzvpdef.pzrpdef_code=code_pzrpdef
and pzrpdef.pzrpdef_code = pzvpdef.pzrpdef_code;

 ret := 'Para el perfil: '||profile_name||' Se registro el: '||TO_CHAR(total, '9999.99')||'%';

return ret;

end get_pzvpdef_sum;

function get_pzvpdef_count
(
 profile_name VARCHAR2
)
return NUMERIC
is
code_pzrpdef VARCHAR2(6);
total numeric;

begin

select  pzrpdef.pzrpdef_code into code_pzrpdef
from pzrpdef
where pzrpdef_name = profile_name;

select COUNT(pzvpdef.pzridef_code) into total 
from pzvpdef,pzrpdef
where pzvpdef.pzrpdef_code=code_pzrpdef
and pzrpdef.pzrpdef_code = pzvpdef.pzrpdef_code;


return total;

end get_pzvpdef_count;

function get_pzvadef_sum
(
 name_inst VARCHAR2, pidm NUMERIC
)
return numeric
is

code_pzridef VARCHAR2(6);
total numeric;

begin

select  pzridef.pzridef_code into code_pzridef
from pzridef
where pzridef_name = name_inst;



select SUM(pzvadef.PZVADEF_DEF_VALUE ) into total 
from pzridef,
     pzvpcod,
     pzrqdef,
     pzvadef,
     pzvidef
where pzridef.pzridef_code= code_pzridef
    and pzridef.pzridef_code=pzvpcod.pzridef_code
    and pzrqdef.pzrqdef_id = pzvpcod.pzrqdef_id
    and pzvpcod.pzvpcod_id = pzvadef.pzvpcod_id
    and pzvadef.pzvidef_id = pzvidef.pzvidef_id
    and pzvidef.pzperso_pidm = pidm;
    --and pzvidef.pzrcdef_code = pzrcdef.pzrcdef_code;
    --and pzvidef.pzrcdef_code= calendar

return total;

end get_pzvadef_sum;

function get_pzvadef_count
(
 name_inst VARCHAR2, pidm NUMERIC
)
return numeric
is

code_pzridef VARCHAR2(6);
total numeric;

begin

select  pzridef.pzridef_code into code_pzridef
from pzridef
where pzridef_name = name_inst;

select COUNT(pzvadef.PZVADEF_DEF_VALUE ) into total 
from pzridef,
     pzvpcod,
     pzrqdef,
     pzvadef,
     pzvidef
where pzridef.pzridef_code= code_pzridef
    and pzridef.pzridef_code=pzvpcod.pzridef_code
    and pzrqdef.pzrqdef_id = pzvpcod.pzrqdef_id
    and pzvpcod.pzvpcod_id = pzvadef.pzvpcod_id
    and pzvadef.pzvidef_id = pzvidef.pzvidef_id
    and pzvidef.pzperso_pidm = pidm;
    --and pzvidef.pzrcdef_code = pzrcdef.pzrcdef_code;
    --and pzvidef.pzrcdef_code= calendar

return total;

end get_pzvadef_count;

function get_percent_pzvadef_obj_reg(name_inst VARCHAR2,
                                                      pidm NUMERIC)
return VARCHAR2
is

resp VARCHAR2(200);
percent_obj_def numeric(8,2);

BEGIN

percent_obj_def  := get_pzvadef_sum('EVALUACI?N DE OBJETIVOS',77460);
--  percent_obj_def  := get_pzvadef_sum(nameinst,pidm);

  if (percent_obj_def= 100) then

   resp:='Ok';


   elsif (percent_obj_def< 100) then 
   resp:='El procentaje de objetivos es inferior a 100%. ('||TO_CHAR(percent_obj_def,'9999.9')||'%)';

   elsif (percent_obj_def> 100) then 
   resp:='El procentaje de objetivos es superior a 100%. ('||TO_CHAR(percent_obj_def,'9999.9')||'%)';

  END IF;

Return resp;

END get_percent_pzvadef_obj_reg;

 PROCEDURE calc_sum_media(pidm numeric,
                                 instr_code VARCHAR2,
                                 calendar VARCHAR2)
IS
  total_num_preguntas numeric;
  suma_respuestas numeric;
  media_respuestas number(8,2);
  
  BEGIN
  -- Instrucciones de ejecuci�n
  
  /*    FORMULAS
  
  	???Ob1+Ob2�Obn=SumObj?

	SumObj/(N�Objetivos )=(?Ob1+Ob2�Obn)/(N�Objetivos)=mediaObj

	mediaObj-ContribObj=DiferenciaObj

  */
  --Calcula el total de preguntas para encontrar la media Y  la sumatoria de los valores
  select COUNT(ins_pre.pzrqdef_id), SUM(resp.PZVADEF_ACCOMP_VALUE) into total_num_preguntas,suma_respuestas
  from PZVADEF resp, pzvpcod ins_pre
  where resp.pzvpcod_id = ins_pre.pzrqdef_id
  and resp.pzperso_pidm ='77460' --IDPersona
  and ins_pre.pzridef_code='EVAOBJ'; --Corresponde al Instrumento

  
  --media de las respuestas
  media_respuestas := suma_respuestas/total_num_preguntas;
  
  DBMS_OUTPUT.PUT_LINE('Datos: ' );
  DBMS_OUTPUT.PUT_LINE('Media de evaluaci�n: ' || media_respuestas); 
  DBMS_OUTPUT.PUT_LINE('N� de Preguntas: ' || total_num_preguntas );
   DBMS_OUTPUT.PUT_LINE('Diferencial No definido '  );
  DBMS_OUTPUT.PUT_LINE('Sumatoria de Respuestas: ' || suma_respuestas);
 
 --diferencial
 
 --resultado
 /*
 INSERT INTO table VALUES (S_PZRADEF.nextval,,);
 VALUES (PZRADEF_ID
PZVADEF_ID
PZRADEF_RESULTA_VALUE
PZRADEF_USER
PZRADEF_ACTIVITY_DATE
PZRADEF_DATA_ORIGIN)*/
  
  END;

PROCEDURE calc_percent_data_obj(pidm numeric,
                                 instr_code VARCHAR2,
                                 resp_id numeric )
IS
  
cursor c_data ( pidm numeric) is
          select pzvadef.pzvadef_accomp_value, pzvadef.pzvadef_def_value 
        from pzvadef,pzvidef,pzvpcod,pzridef
        where pzvadef.pzvidef_id = pzvidef.pzvidef_id
        and pzvadef.pzvpcod_id = pzvpcod.pzvpcod_id
        and pzvpcod.pzridef_code = pzridef.pzridef_code
        and pzridef.pzridef_name ='EVALUACI?N DE OBJETIVOS'
        and pzvidef.pzperso_pidm =77460
        AND pzvadef.pzvadef_description not in('Contribuci�n global al logro de Objetivos');
        --and pzridef.pzridef_name =instr_code
        --and pzvidef.pzperso_pidm =pidm;
 
  obj_value number(8,2);
  ret_value number (8,2);
  percent_obj_def number(8,2);
  count_obj number;
  total_value_obj integer :=0;
  obj_value_me number (8,2);
  total_value_perc integer:=0;
  val_obj integer :=0;
  dif_value number :=0;
  dif number;
  tot_perc_obj number;
  percent_profile number(8,2);
  
  BEGIN

  --verificar porcentaje de objetivos 
  percent_obj_def  := get_pzvadef_sum('EVALUACI?N DE OBJETIVOS',77460);

  --verificar numero de objetivos
  count_obj := get_pzvadef_count('EVALUACI?N DE OBJETIVOS',77460);

  if ( percent_obj_def =100 ) then
      --Buscar Objetivos
     -- counter :=1;
      OPEN c_data(pidm);
     
        loop        
        Fetch c_data into obj_value,percent_obj_def;
        EXIT WHEN c_data%NOTFOUND;
        
        --Sumatoria de Objetivos
         total_value_obj:=total_value_obj+obj_value;
        
        --Sumatoria de Porcentajes
        total_value_perc:=total_value_perc+percent_obj_def;
        DBMS_OUTPUT.PUT_LINE('Porcentaje calculado: '|| total_value_perc ||'% Valor del item evaluado: '|| obj_value);
       
        
      end loop; 
        
        close c_data;
        
        
        val_obj :=count_obj - 1;
        --Valor de la Media
        obj_value_me :=total_value_obj/val_obj;
        
        --Hallar el resultado de contibucion al logro (Cuyo porcentaje siempre es 0)
        select pzvadef.pzvadef_accomp_value into dif
        from pzvadef,pzvidef,pzvpcod,pzridef
        where pzvadef.pzvidef_id = pzvidef.pzvidef_id
        and pzvadef.pzvpcod_id = pzvpcod.pzvpcod_id
        and pzvpcod.pzridef_code = pzridef.pzridef_code
        and pzridef.pzridef_name ='EVALUACI?N DE OBJETIVOS'
        and pzvidef.pzperso_pidm =77460
        AND pzvadef.pzvadef_description in('Contribuci�n global al logro de Objetivos');
        
        dif_value := obj_value_me-dif;
        DBMS_OUTPUT.PUT_LINE('Contribuci�n global al logro de Objetivos: '|| dif);
        DBMS_OUTPUT.PUT_LINE(' ');
        DBMS_OUTPUT.PUT_LINE('Datos: ');
        DBMS_OUTPUT.PUT_LINE('N� de Objetivos Evaluados: '|| val_obj);
        DBMS_OUTPUT.PUT_LINE('Sumatoria de Objetivos: '|| total_value_obj);
        DBMS_OUTPUT.PUT_LINE('Media: '|| obj_value_me);
        DBMS_OUTPUT.PUT_LINE('DIFERENCIA (R. Objetivos-Contribucion global al logro de Objetivos): '||dif_value);
  
  
  
  --***********************************************INSERT base 100
  
  --Aqui sacamos el valor en BASE a lo que tenga cargado el perfil de usuario
  select DISTINCT( pzvpdef.pzvpdef_value) into percent_profile
    from pzvpdef,pzrpdef,pzvldef,pzridef
    where pzvpdef.pzridef_code = pzridef.pzridef_code
          and pzvpdef.pzrpdef_code = pzrpdef.pzrpdef_code
          and pzrpdef.pzrpdef_code = pzvldef.pzrpdef_code
          and pzvldef.pzperso_pidm =77460
          and pzridef.pzridef_name='EVALUACI?N DE OBJETIVOS';
  
  tot_perc_obj:=obj_value_me*(percent_profile/100);
  
  DBMS_OUTPUT.PUT_LINE('Resultado en Base al '||percent_profile||'%: '||tot_perc_obj);
  end if;
  
  END;

--Devuelve el porcentaje de un perfil
function get_pzvpdef_data
(
 name_inst VARCHAR2, pidm NUMERIC,profile_us VARCHAR2
)
return NUMERIC
is
percent_profile numeric;

BEGIN
  select DISTINCT( pzvpdef.pzvpdef_value) into percent_profile
    from pzvpdef,pzrpdef,pzvldef,pzridef
    where pzvpdef.pzridef_code = pzridef.pzridef_code
          and pzvpdef.pzrpdef_code = pzrpdef.pzrpdef_code
          and pzrpdef.pzrpdef_code = pzvldef.pzrpdef_code
          and pzvldef.pzperso_pidm =pidm
          and pzridef.pzridef_name=name_inst
          and pzrpdef.pzrpdef_code=profile_us;

return percent_profile;
end get_pzvpdef_data;





END PACKAGE1;

/
