--------------------------------------------------------
-- Archivo creado  - mi�rcoles-octubre-30-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure CALC_PERCENT_DATA_COMP
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "MPEREZFI"."CALC_PERCENT_DATA_COMP" (pidm numeric,
                                 instr_code VARCHAR2,
                                 resp_id numeric )
IS
  
cursor c_data ( pidm numeric) is
          select pzvadef.pzvadef_accomp_value, pzvadef.pzvadef_def_value 
        from pzvadef,pzvidef,pzvpcod,pzridef
        where pzvadef.pzvidef_id = pzvidef.pzvidef_id
        and pzvadef.pzvpcod_id = pzvpcod.pzvpcod_id
        and pzvpcod.pzridef_code = pzridef.pzridef_code
        and pzridef.pzridef_name ='EVALUACI?N DE COMPETENCIAS GENERICAS'
        and pzvidef.pzperso_pidm =77460
        AND pzvadef.pzvadef_description not in('Competencia Estimada (Global)');
        --and pzridef.pzridef_name =instr_code
        --and pzvidef.pzperso_pidm =pidm;

  comp_value number(8,2);
  ret_value number (8,2);
  percent_comp_def number(8,2);
  count_comp number;
  total_value_comp integer :=0;
  comp_value_me number (8,2);
  total_value_perc integer:=0;
  val_comp integer :=0;
  dif_value number :=0;
  dif number;
  tot_perc_comp number;
  percent_profile number(8,2);

  BEGIN
 
  --verificar numero de competencias
  count_comp := get_pzvadef_count('EVALUACI?N DE COMPETENCIAS GENERICAS',77460);
 
  if ( count_comp >0 ) then
      --Buscar competencias
     -- counter :=1;
      OPEN c_data(pidm);

        loop        
        Fetch c_data into comp_value,percent_comp_def;
        EXIT WHEN c_data%NOTFOUND;

        --Sumatoria de competencias
         total_value_comp:=total_value_comp+comp_value;

        
         DBMS_OUTPUT.PUT_LINE(' Valor del item evaluado: '|| comp_value);
       

      end loop; 

        close c_data;


        val_comp :=count_comp - 1;
        --Valor de la Media
        comp_value_me :=total_value_comp/val_comp;

        --Hallar el resultado de contibucion al logro (Cuyo porcentaje siempre es 0)
        select pzvadef.pzvadef_accomp_value into dif
        from pzvadef,pzvidef,pzvpcod,pzridef
        where pzvadef.pzvidef_id = pzvidef.pzvidef_id
        and pzvadef.pzvpcod_id = pzvpcod.pzvpcod_id
        and pzvpcod.pzridef_code = pzridef.pzridef_code
        and pzridef.pzridef_name ='EVALUACI?N DE COMPETENCIAS GENERICAS'
        and pzvidef.pzperso_pidm =77460
        AND pzvadef.pzvadef_description in('Competencia Estimada (Global)');

        dif_value := comp_value_me-dif;
        
        
        DBMS_OUTPUT.PUT_LINE('Competencia Estimada (Global): '|| dif);
        DBMS_OUTPUT.PUT_LINE(' ');
        DBMS_OUTPUT.PUT_LINE('Datos: ');
        DBMS_OUTPUT.PUT_LINE('N� de competencias Evaluadas: '|| val_comp);
        DBMS_OUTPUT.PUT_LINE('Sumatoria de competencias: '|| total_value_comp);
        DBMS_OUTPUT.PUT_LINE('Media: '|| comp_value_me);
        DBMS_OUTPUT.PUT_LINE('DIFERENCIA (competencia Real-Competencia Estimada): '||dif_value);



  --***********************************************INSERT base 100

  ---


  --Aqui sacamos el valor en BASE a lo que tenga cargado el perfil de usuario
  select DISTINCT( pzvpdef.pzvpdef_value) into percent_profile
    from pzvpdef,pzrpdef,pzvldef,pzridef
    where pzvpdef.pzridef_code = pzridef.pzridef_code
          and pzvpdef.pzrpdef_code = pzrpdef.pzrpdef_code
          and pzrpdef.pzrpdef_code = pzvldef.pzrpdef_code
          and pzvldef.pzperso_pidm =77460
          and pzridef.pzridef_name='EVALUACI?N DE COMPETENCIAS GENERICAS';

  tot_perc_comp:=comp_value_me*(percent_profile/100);

  
  DBMS_OUTPUT.PUT_LINE('Resultado en Base al '||percent_profile||'%: '||tot_perc_comp);
  end if;

  END;

/
